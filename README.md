# Patrones-grupoB




## Build project

```docker build -t org/pas:ejercicio1-0-0 .```

## Execute project 

```docker run -p 8080:8080 org/pas:ejercicio1-0-0```


### Docker compose 


Teniendo Docker compose instalado se puede levantar con este comando

```docker-compose up ```

Y para re buildea cuando se hace un cambio en el código podemos usar 

``` docker-compose up --build patrones2023```

## Endpoints


### Crear Persona

con ```POST``` al endpoint ```http://localhost:8080/api/person``` json de ejemplo:

```json
{
	"firstName": "Santiago",
	"lastName": "Makcimovich",
	"birth": "1997-02-16"
}
```

### Listar Personas


con ```GET``` al endpoint ```http://localhost:8080/api/person``` 




### ENVIRONMENT FILE

Es necesario en el root crear un archivo ```.env``` para poder instanciar las variables de entorno este debe tener la variable 

```sh
SECRET_KEY=MySecretKeyHere
ENVRIOMENT=dev
``` 

## SWAGGER

La url para entra a swagger es la siguiente, para que esto funcione se debe tener configurado la variable ```ENVIORMENT``` en el ```.env``` como ```dev```

```sh
http://localhost:8080/swagger-ui.html
```