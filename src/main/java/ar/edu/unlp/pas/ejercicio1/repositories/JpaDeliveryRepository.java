package ar.edu.unlp.pas.ejercicio1.repositories;

import ar.edu.unlp.pas.ejercicio1.models.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaDeliveryRepository extends JpaRepository<Delivery, Long> {
    List<Delivery> findByPersonId(long personId);
}
