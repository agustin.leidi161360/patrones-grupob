package ar.edu.unlp.pas.ejercicio1.dtos.Publication;

import javax.validation.constraints.NotNull;

public class PublicationModifyStockDto {

    @NotNull(message="Debe ingresar la publicacion que quiere modificar")
    private Integer publicationId;

    @NotNull(message="Debe ingresar un stock")
    private Integer cantProducts;

    

    public PublicationModifyStockDto() {
    }

    public PublicationModifyStockDto(Integer publicationId, Integer cantProducts) {
        this.publicationId = publicationId;
        this.cantProducts = cantProducts;
    }

    public Integer getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(Integer publicationId) {
        this.publicationId = publicationId;
    }

    public Integer getcantProducts() {
        return cantProducts;
    }

    public void setcantProducts(Integer cantProducts) {
        this.cantProducts = cantProducts;
    }
    
}
