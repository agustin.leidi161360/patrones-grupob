package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import ar.edu.unlp.pas.ejercicio1.models.Delivery;
import ar.edu.unlp.pas.ejercicio1.models.Publication;

import java.util.List;
import java.util.Set;

public interface IDeliveryService {

    Delivery getDeliveryById(long id);

    List<Delivery> getAllDeliveriesByPersonId(long id);

    Set<Publication> getAllPublicationsByDeliveryId(long id);

    List<Delivery> getDeliveries();
}
