package ar.edu.unlp.pas.ejercicio1.dtos.ShoppingCart;

import java.util.HashSet;
import java.util.Set;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationDto;

public class ShoppingCartDto {

    private Long id;
    private Set<PublicationDto> publications = new HashSet<>();

    public ShoppingCartDto() {

    }

    public ShoppingCartDto(Long id, Set<PublicationDto> publications) {
        this.id = id;
        this.publications = publications;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<PublicationDto> getPublications() {
        return publications;
    }

    public void setPublications(Set<PublicationDto> publications) {
        this.publications = publications;
    }

}