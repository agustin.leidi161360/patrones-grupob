package ar.edu.unlp.pas.ejercicio1.controllers;

import ar.edu.unlp.pas.ejercicio1.dtos.Person.PersonCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Person.PersonGetDto;
import ar.edu.unlp.pas.ejercicio1.models.Person;
import ar.edu.unlp.pas.ejercicio1.services.implementations.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PersonController {

    @Inject
    private PersonService personService;

    @PostMapping("/api/person")
    public ResponseEntity<Object> createPerson(@Valid @RequestBody PersonCreateDto personDto) {
        try {
            Person person = personService.createPerson(personDto);
            return ResponseEntity.ok(person);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage() + " //// " + e);
        }
    }

    @GetMapping("/api/person")
    public ResponseEntity<List<PersonGetDto>> listPersons() {
        List<Person> persons = personService.listPersons();
        List<PersonGetDto> personsGetDTO = new ArrayList<>();

        for (Person person : persons) {
            PersonGetDto personGetDTO = new PersonGetDto(person);
            personsGetDTO.add(personGetDTO);
        }
        return ResponseEntity.ok(personsGetDTO);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
