package ar.edu.unlp.pas.ejercicio1.controllers;

import ar.edu.unlp.pas.ejercicio1.dtos.SystemConfig.SystemConfigGetDto;
import ar.edu.unlp.pas.ejercicio1.dtos.SystemConfig.SystemConfigUpdateDto;
import ar.edu.unlp.pas.ejercicio1.models.SystemConfig;
import ar.edu.unlp.pas.ejercicio1.services.implementations.SystemConfigService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller
public class SystemConfigController {
    @Inject
    private SystemConfigService systemConfigService;

    @PatchMapping("/api/systemConfig")
    public ResponseEntity<Object> updateSystemConfigs(@Valid @RequestBody SystemConfigUpdateDto systemConfigUpdateDto) {
        try {
            return ResponseEntity.ok(systemConfigService.updateSystemConfig(systemConfigUpdateDto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage() + " //// " + e);
        }
    }

    @GetMapping("/api/systemConfig")
    public ResponseEntity<SystemConfigGetDto> getSystemConfig() {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        SystemConfigGetDto systemConfigGetDto = new SystemConfigGetDto(systemConfig.getCommission(), systemConfig.getMaxOpinionsToShow());
        return ResponseEntity.ok(systemConfigGetDto);
    }
}
