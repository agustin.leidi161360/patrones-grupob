package ar.edu.unlp.pas.ejercicio1.controllers;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationDto;
import ar.edu.unlp.pas.ejercicio1.dtos.ShoppingCart.AddProductDto;
import ar.edu.unlp.pas.ejercicio1.dtos.ShoppingCart.ShoppingCartDto;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.models.ShoppingCart;
import ar.edu.unlp.pas.ejercicio1.services.implementations.ShoppingCartService;

@RestController
public class ShoppingCartController {

    @Inject
    private ShoppingCartService shoppingCartService;

    @GetMapping("/api/shopping-cart")
    public ResponseEntity<ShoppingCartDto> getShoppingCart(Authentication authentication, HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        ShoppingCart shoppingCart = shoppingCartService.getShoppingCart(personId);
        Set<PublicationDto> publicationsDto = new HashSet<>();
        for (Publication publication : shoppingCart.getPublications()) {
            PublicationDto publicationDto = new PublicationDto(publication);
            publicationsDto.add(publicationDto);
        }
        ShoppingCartDto shoppingCartDto = new ShoppingCartDto(
                shoppingCart.getId(),
                publicationsDto);
        return ResponseEntity.ok(shoppingCartDto);
    }

    @PostMapping("/api/shopping-cart/clear")
    public ResponseEntity<Boolean> clearShoppingCart(Authentication authentication,
            HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        return ResponseEntity.ok(shoppingCartService.clearShoppingCart(personId));
    }

    @PostMapping("/api/shopping-cart")
    public ResponseEntity<Boolean> addProductToShoppingCart(Authentication authentication,
            HttpServletRequest request,
            @RequestBody AddProductDto body) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        long publicationIdLong = Long.parseLong(body.publicationId);
        return ResponseEntity.ok(shoppingCartService.addProductToShopingCart(personId, publicationIdLong));
    }

    @DeleteMapping("/api/shopping-cart")
    public ResponseEntity<Boolean> removeProductFromShoppingCart(Authentication authentication,
            HttpServletRequest request,
            @RequestBody AddProductDto body) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        long publicationIdLong = Long.parseLong(body.publicationId);
        return ResponseEntity.ok(shoppingCartService.removeProductFromShoppingCart(personId, publicationIdLong));
    }
}
