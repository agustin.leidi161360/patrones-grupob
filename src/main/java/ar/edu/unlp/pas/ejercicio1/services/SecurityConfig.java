package ar.edu.unlp.pas.ejercicio1.services;

import ar.edu.unlp.pas.ejercicio1.services.implementations.PersonService;
import ar.edu.unlp.pas.ejercicio1.services.security.JWTAuthorizationFilter;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PersonService personService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        Dotenv dotenv = Dotenv.configure().load();
        String env = dotenv.get("ENVIRONMENT");

        if (env.equals("dev")) {
            http.authorizeRequests()
                    .antMatchers("/h2-console/**", "/swagger-ui.html/**", "/webjars/springfox-swagger-ui/**",
                            "/swagger-resources/**", "/v2/api-docs/**")
                    .permitAll()
                    .and()
                    .csrf()
                    .ignoringAntMatchers("/h2-console/**", "/swagger-ui.html/**", "/webjars/springfox-swagger-ui/**",
                            "/swagger-resources/**",
                            "/v2/api-docs/**")
                    .and()
                    .headers()
                    .frameOptions()
                    .sameOrigin();
            this.configureAuthRequests(http);
        }

        http.csrf().disable()
                .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/auth/**").permitAll()
                .anyRequest().authenticated();

    }

    private void configureAuthRequests(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(HttpMethod.GET).hasAnyAuthority("USER", "ADMIN", "SELLER", "DELIVERER");
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/auth/login").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/shopping-cart").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/address").hasAuthority("ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/person").hasAuthority("ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.PATCH, "/api/systemConfig").hasAuthority("ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/publications").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/publications").hasAuthority("SELLER");
        http.authorizeRequests().antMatchers(HttpMethod.PUT, "/api/publications").hasAuthority("SELLER");
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/products").hasAnyAuthority("SELLER", "ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/products").hasAuthority("SELLER");
        http.authorizeRequests().antMatchers(HttpMethod.PUT, "/api/products").hasAuthority("SELLER");
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/deliveries").hasAnyAuthority("USER", "DELIVERER");
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/reports").hasAuthority("ADMIN");

    }
}
