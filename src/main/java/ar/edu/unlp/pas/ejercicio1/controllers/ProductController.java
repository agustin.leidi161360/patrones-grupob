package ar.edu.unlp.pas.ejercicio1.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import ar.edu.unlp.pas.ejercicio1.dtos.Product.ProductCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Product.ProductGetDto;
import ar.edu.unlp.pas.ejercicio1.models.Product;
import ar.edu.unlp.pas.ejercicio1.services.implementations.ProductService;
import ar.edu.unlp.pas.ejercicio1.services.implementations.PublicationService;

@RestController
public class ProductController {

    @Inject
    ProductService prodService;
    @Inject
    PublicationService pubService;

    @GetMapping("/api/products") 
    public ResponseEntity<List<ProductGetDto>> getProducts() {
            List<Product> products = prodService.getProducts();
            List<ProductGetDto> prodsGetDto = new ArrayList<>();
            for (Product product : products) {
                ProductGetDto productGetDto = new ProductGetDto(product);
                prodsGetDto.add(productGetDto);
            }

            return ResponseEntity.ok(prodsGetDto);


    }

    @PostMapping("/api/products")
    public ResponseEntity<ProductGetDto> createProduct(@Valid @RequestBody ProductCreateDto productCreateDto) {
            Product product = prodService.addProduct(productCreateDto);
            ProductGetDto prod = new ProductGetDto(product);

            return ResponseEntity.ok(prod);

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


}
