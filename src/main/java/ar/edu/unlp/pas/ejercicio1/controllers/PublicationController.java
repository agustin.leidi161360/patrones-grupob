package ar.edu.unlp.pas.ejercicio1.controllers;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationGetDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationModifyStockDto;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.services.implementations.PublicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PublicationController {

    @Inject
    PublicationService pubService;

    @GetMapping("/api/publications")
    public ResponseEntity<List<PublicationGetDto>> getPublications() {

        List<Publication> publications = pubService.getPublications();
        List<PublicationGetDto> pubs = new ArrayList<>();
        for (Publication publication : publications) {
            PublicationGetDto publicationGetDto = new PublicationGetDto(publication);
            pubs.add(publicationGetDto);
        }

        return ResponseEntity.ok(pubs);
    }

    @GetMapping("/api/publications/{personId}")
    public ResponseEntity<List<PublicationGetDto>> getPublicationsByPersonId(@PathVariable("personId") Integer personId) {

        List<Publication> publications = pubService.getPublicationsOfPerson(personId);
        List<PublicationGetDto> pubs = new ArrayList<>();
        for (Publication publication : publications) {
            PublicationGetDto publicationGetDto = new PublicationGetDto(publication);
            pubs.add(publicationGetDto);
        }

        return ResponseEntity.ok(pubs);
    }


    @PostMapping("/api/publications")
    public ResponseEntity<PublicationGetDto> createPublication(@Valid @RequestBody PublicationCreateDto publicationDto) {

        Publication publication = pubService.addPublication(publicationDto);
        PublicationGetDto publicationGetDto = new PublicationGetDto(publication);

        return ResponseEntity.ok(publicationGetDto);
    }

    @PutMapping("/api/publications/stock")
    public ResponseEntity<PublicationGetDto> modifyStock(@Valid @RequestBody PublicationModifyStockDto publicationDto) {

        Publication publiModified = pubService.modifyStock(publicationDto.getPublicationId(), publicationDto.getcantProducts());
        PublicationGetDto publicationGetDto = new PublicationGetDto(publiModified);

        return ResponseEntity.ok(publicationGetDto);
    }

    @PatchMapping("api/publications/suspend/{idPublication}")
    public ResponseEntity<PublicationGetDto> suspendPublication(@PathVariable("idPublication") Long idPublication) {

        Publication publication = pubService.suspendPublication(idPublication);
        PublicationGetDto publicationGetDto = new PublicationGetDto(publication);

        return ResponseEntity.ok(publicationGetDto);
    }
}
