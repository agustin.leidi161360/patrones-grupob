package ar.edu.unlp.pas.ejercicio1.services.implementations;

import ar.edu.unlp.pas.ejercicio1.models.Checkout;
import ar.edu.unlp.pas.ejercicio1.models.ShoppingCart;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaCheckoutRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.ICheckoutService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Service
public class CheckoutService implements ICheckoutService {

    @Inject
    private JpaCheckoutRepository checkoutRepository;
    @Inject
    private ShoppingCartService shoppingCartService;
    @Inject
    private PersonService personService;

    @Override
    public Checkout createCheckout(long personId) {
        ShoppingCart cart = shoppingCartService.getShoppingCart(personId);
        if (cart.getPublications().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El carrito de compra está vacío");
        }
        Checkout checkout = new Checkout(new Date(), cart.getPublications(), personService.getPersonById(personId));
        checkoutRepository.save(checkout);
        shoppingCartService.clearShoppingCart(personId);
        return checkout;
    }

    @Override
    public Checkout getCheckoutById(long id) {
        return checkoutRepository.findById(id).orElse(null);
    }

    @Override
    public List<Checkout> getAllCheckoutsByPersonId(long personId) {
        return checkoutRepository.findByPersonId(personId);
    }
}
