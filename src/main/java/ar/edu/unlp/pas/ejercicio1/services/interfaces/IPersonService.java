package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import ar.edu.unlp.pas.ejercicio1.dtos.Person.PersonCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Person.PersonGetDto;
import ar.edu.unlp.pas.ejercicio1.models.Person;

import java.util.List;

public interface IPersonService {
    Person createPerson(PersonCreateDto personDto) throws Exception;

    List<Person> listPersons();

    Person getPersonByEmail(String email);

    boolean existsPersonByEmail(String email);

    boolean authenticateUser(String email, String password);

    Person getPersonById(Long id);
}
