package ar.edu.unlp.pas.ejercicio1.services.implementations;

import java.util.Date;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.ejercicio1.dtos.Reports.ReportSalesByMonthDTO;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaCheckoutRepository;
import ar.edu.unlp.pas.ejercicio1.repositories.implementations.JpaReportsRepositoryImp;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.IReportsService;

@Service
public class ReportsService implements IReportsService {

    @Inject
    private JpaCheckoutRepository checkoutRepository;

    @Inject
    private JpaReportsRepositoryImp reportsRepository;

    public ReportSalesByMonthDTO reportSalesByMonth(Integer month, Integer year) {

        ReportSalesByMonthDTO result = this.checkoutRepository.reportSalesByMonth(month, year);
        if (result == null) {
            throw new RuntimeException("No se encontraron ventas para el mes y año indicados");
        }
        return result;
    }

    public Integer reportSalesBetweenAge(int minAge, int maxAge) {
        Integer result = this.checkoutRepository.reportSalesBetweenAge(minAge, maxAge);
        if (result == null) {
            throw new RuntimeException("No se encontraron ventas para el rango de edad indicado");
        }
        return result;
    }

    public Integer reportAmount(
            Integer productId,
            String category,
            Integer buyerId,
            Integer sellerId,
            Date dateStart,
            Date dateEnd) {

        return this.reportsRepository.reportAmount(
                productId,
                category,
                buyerId,
                sellerId,
                dateStart,
                dateEnd);

    }

}
