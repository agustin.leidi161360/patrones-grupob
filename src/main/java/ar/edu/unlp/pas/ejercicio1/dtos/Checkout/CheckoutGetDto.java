package ar.edu.unlp.pas.ejercicio1.dtos.Checkout;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationDto;

import java.util.Date;
import java.util.Set;

public class CheckoutGetDto {

    private Long id;
    private Date dateCheckout;
    private Set<PublicationDto> publications;

    public CheckoutGetDto(Long id, Date dateCheckout, Set<PublicationDto> publications) {
        this.id = id;
        this.dateCheckout = dateCheckout;
        this.publications = publications;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCheckout() {
        return dateCheckout;
    }

    public void setDateCheckout(Date dateCheckout) {
        this.dateCheckout = dateCheckout;
    }

    public Set<PublicationDto> getPublications() {
        return publications;
    }

    public void setPublications(Set<PublicationDto> publications) {
        this.publications = publications;
    }
}
