package ar.edu.unlp.pas.ejercicio1.controllers;

import ar.edu.unlp.pas.ejercicio1.dtos.Checkout.CheckoutGetDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationDto;
import ar.edu.unlp.pas.ejercicio1.models.Checkout;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.services.implementations.CheckoutService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class CheckOutController {

    @Inject
    private CheckoutService checkoutService;

    @GetMapping("/api/checkouts")
    public ResponseEntity<List<CheckoutGetDto>> listCheckoutsByPersonId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        List<Checkout> checkouts = checkoutService.getAllCheckoutsByPersonId(personId);
        List<CheckoutGetDto> checkoutsGetDto = new ArrayList<>();
        for (Checkout checkout : checkouts) {
            Set<PublicationDto> publicationsDto = new HashSet<>();
            for (Publication publication : checkout.getPublications()) {
                PublicationDto publicationDto = new PublicationDto(publication);
                publicationsDto.add(publicationDto);
            }
            CheckoutGetDto checkoutGetDto = new CheckoutGetDto(checkout.getId(), checkout.getDateCheckout(), publicationsDto);
            checkoutsGetDto.add(checkoutGetDto);
        }
        return ResponseEntity.ok(checkoutsGetDto);
    }

    @PostMapping("/api/checkouts")
    public ResponseEntity<CheckoutGetDto> createCheckout() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        Checkout checkout = checkoutService.createCheckout(personId);
        Set<PublicationDto> publicationsDto = new HashSet<>();
        for (Publication publication : checkout.getPublications()) {
            PublicationDto publicationDto = new PublicationDto(publication);
            publicationsDto.add(publicationDto);
        }
        CheckoutGetDto checkoutGetDto = new CheckoutGetDto(checkout.getId(), checkout.getDateCheckout(), publicationsDto);
        return ResponseEntity.ok(checkoutGetDto);
    }
}
