package ar.edu.unlp.pas.ejercicio1.dtos.Reports;

import javax.persistence.Column;

public interface ReportSalesByMonthDTO {

    @Column(name = "MONTH")
    public Integer getMonth();

    @Column(name = "SALES")
    public Integer getSales();

}
