package ar.edu.unlp.pas.ejercicio1.dtos.ShoppingCart;

public class AddProductDto {

    public String publicationId;

    public AddProductDto() {

    }

    public AddProductDto(String publicationId) {
        this.publicationId = publicationId;
    }

    public String getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(String publicationId) {
        this.publicationId = publicationId;
    }
}
