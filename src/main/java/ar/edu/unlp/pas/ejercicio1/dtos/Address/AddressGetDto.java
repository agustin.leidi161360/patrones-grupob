package ar.edu.unlp.pas.ejercicio1.dtos.Address;

import ar.edu.unlp.pas.ejercicio1.models.Address;

public class AddressGetDto {
    private Integer street;
    private Integer houseNumber;
    private Integer floorNumber;
    private Integer department;
    private String city;
    private String province;
    private String country;
    private Long person_id;

    public AddressGetDto() {
    }

    public AddressGetDto(Address address) {
        this.street = address.getStreet();
        this.houseNumber = address.getHouseNumber();
        this.floorNumber = address.getFloorNumber();
        this.department = address.getDepartment();
        this.city = address.getCity();
        this.province = address.getProvince();
        this.country = address.getCountry();
        this.person_id = address.getPerson().getId();
    }

    public Integer getStreet() {
        return street;
    }

    public void setStreet(Integer street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(Integer floorNumber) {
        this.floorNumber = floorNumber;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getPersonId() {
        return person_id;
    }

    public void setPersonId(Long person_id) {
        this.person_id = person_id;
    }

}
