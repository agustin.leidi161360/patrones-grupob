package ar.edu.unlp.pas.ejercicio1.models;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "checkout")
public class Checkout {

    @Id
    @Column(nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date", nullable = false)
    private Date dateCheckout;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "publication_checkout",
            joinColumns = @JoinColumn(name = "publication_id"),
            inverseJoinColumns = @JoinColumn(name = "checkout_id"))
    private Set<Publication> publications = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "person_id", nullable = false)
    private Person person;


    public Checkout() {
    }

    public Checkout(Date dateCheckout, Set<Publication> publications, Person person) {
        this.dateCheckout = dateCheckout;
        this.publications.addAll(publications);
        this.person = person;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCheckout() {
        return dateCheckout;
    }

    public void setDateCheckout(Date dateCheckout) {
        this.dateCheckout = dateCheckout;
    }

    public Set<Publication> getPublications() {
        return publications;
    }

    public void setPublications(Set<Publication> publications) {
        this.publications = publications;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
