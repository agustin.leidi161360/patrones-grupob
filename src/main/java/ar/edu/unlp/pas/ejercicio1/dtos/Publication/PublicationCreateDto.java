package ar.edu.unlp.pas.ejercicio1.dtos.Publication;

import javax.validation.constraints.NotNull;

public class PublicationCreateDto {

    @NotNull(message = "Debe tener un producto")
    Integer productId;

    @NotNull(message = "El campo precio de venta debe tener un valor")
    Double sellPrice;

    @NotNull(message = "El campo precio minimo debe tener un valor")
    Double minPrice;

    @NotNull(message = "El campo precio maximo debe tener un valor")
    Double maxPrice;

    @NotNull(message = "El campo stock debe tener un valor")
    Integer stock;

    @NotNull(message = "Debe indicar si inicia activa o no")
    Boolean isActive;


    

    public PublicationCreateDto(Integer productId,
            Double sellPrice,
            Double minPrice,
            Double maxPrice,
            Integer stock,
            Boolean isActive) {
        this.productId = productId;
        this.sellPrice = sellPrice;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.stock = stock;
        this.isActive = isActive;
    }

    public PublicationCreateDto() {
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

}
