package ar.edu.unlp.pas.ejercicio1;

import ar.edu.unlp.pas.ejercicio1.dtos.Address.AddressCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Person.PersonCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Product.ProductCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationCreateDto;
import ar.edu.unlp.pas.ejercicio1.enums.UserRole;
import ar.edu.unlp.pas.ejercicio1.models.Person;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.models.SystemConfig;
import ar.edu.unlp.pas.ejercicio1.services.implementations.*;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;

@Component
@Transactional
public class InitDatabase implements ApplicationRunner {

    @Inject
    private PersonService pService;
    @Inject
    private ProductService prodService;
    @Inject
    private PublicationService pubService;
    @Inject
    private CheckoutService checkoutService;
    @Inject
    private ShoppingCartService shoppingCartService;
    @Inject 
    private DeliveryService deliveryService;

    @Inject
    private SystemConfigService systemConfigService;

    @Override
    public void run(ApplicationArguments args) {

        try {
            Random rand = new Random();

            AddressCreateDto address1 = new AddressCreateDto(
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    "Toronto",
                    "Ontario",
                    "Canada");

            AddressCreateDto address2 = new AddressCreateDto(
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    "Vancouver",
                    "British Columbia",
                    "Canada");

            AddressCreateDto address3 = new AddressCreateDto(
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    "Calgary",
                    "Alberta",
                    "Canada");

            AddressCreateDto address4 = new AddressCreateDto(
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    rand.nextInt(),
                    "La Plata",
                    "Buenos Aires",
                    "Argentina");

            HashSet<AddressCreateDto> set1 = new HashSet<>();
            set1.add(address1);
            set1.add(address2);

            HashSet<AddressCreateDto> set2 = new HashSet<>();
            set2.add(address3);
            set2.add(address2);

            HashSet<AddressCreateDto> set3 = new HashSet<>();
            set3.add(address4);

            PersonCreateDto p1 = new PersonCreateDto("Carlos", "Perez", LocalDate.parse("1993-02-16"),
                    "carlo@gmail.com", UserRole.ADMIN.getRole(), "123", address1, set1);
            PersonCreateDto p2 = new PersonCreateDto("Juan", "Perez", LocalDate.parse("1993-02-16"),
                    "juan@gmail.com",
                    UserRole.USER.getRole(), "123", address2, set2);
            PersonCreateDto p3 = new PersonCreateDto("Pedro", "Perez", LocalDate.parse("1997-02-16"),
                    "pedro@gmail.com", UserRole.USER.getRole(), "123", address3, set2);
            PersonCreateDto p4 = new PersonCreateDto("Roman", "Seller", LocalDate.parse("1969-05-24"),
                    "seller@gmail.com", UserRole.SELLER.getRole(), "123", address4, set3);
            PersonCreateDto p5 = new PersonCreateDto("Juan", "Rodriguez", LocalDate.parse("1996-05-24"),
                "juanrodriguez@gmail.com", UserRole.DELIVERER.getRole(), "123", address4, set3);

            pService.createPerson(p1);
            pService.createPerson(p2);
            pService.createPerson(p3);
            pService.createPerson(p4);
            pService.createPerson(p5);

            ProductCreateDto prod1 = new ProductCreateDto("Remera xl", "Es una remera", 500.40,
                    "Vestimenta", Long.valueOf((long) 4));
            PublicationCreateDto pub1 = new PublicationCreateDto(1, 200.00, 100.00, 350.00, 50, true);

            prodService.addProduct(prod1);
            pubService.addPublication(pub1);
            pubService.modifyStock(1, -10);
            Person person = pService.getPersonByEmail(p3.getEmail());
            Publication pub = pubService.getPublications().get(0);

            shoppingCartService.addProductToShopingCart(person.getId(), pub.getId());

            shoppingCartService.getShoppingCart(person.getId());

            // revisar !! 
            checkoutService.createCheckout(person.getId());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
