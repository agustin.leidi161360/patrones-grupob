package ar.edu.unlp.pas.ejercicio1.dtos.Publication;
import ar.edu.unlp.pas.ejercicio1.models.Product;
import ar.edu.unlp.pas.ejercicio1.models.Publication;

public class PublicationGetDto {

    private Integer stock;

    private Double sellPrice;

    private Double minPrice;

    private Double maxPrice;

    public PublicationGetDto() {
    }


    private Boolean isActive;

    private Long productId;



    public PublicationGetDto(Publication pub) {
        this.stock = pub.getStock();
        this.sellPrice = pub.getSellPrice();
        this.minPrice = pub.getMinPrice();
        this.maxPrice = pub.getMaxPrice();
        this.isActive = pub.getIsActive();
        this.productId = pub.getProduct().getId();
    }


    public Integer getStock() {
        return stock;
    }


    public void setStock(Integer stock) {
        this.stock = stock;
    }


    public Double getSellPrice() {
        return sellPrice;
    }


    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }


    public Double getMinPrice() {
        return minPrice;
    }


    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }


    public Double getMaxPrice() {
        return maxPrice;
    }


    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }


    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public Long getProductId() {
        return productId;
    }


    public void setProductId(Long productId) {
        this.productId = productId;
    }


    
}
