package ar.edu.unlp.pas.ejercicio1.services.implementations;

import ar.edu.unlp.pas.ejercicio1.dtos.Address.AddressCreateDto;
import ar.edu.unlp.pas.ejercicio1.models.Address;
import ar.edu.unlp.pas.ejercicio1.models.Person;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaAddressRepository;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaPersonRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.IAddressService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
@Transactional
public class AddressService implements IAddressService {

    @Inject
    private JpaAddressRepository jpaAddressRepository;

    @Inject
    private JpaPersonRepository jpaPersonRepository;

    @Override
    public Address createAddress(AddressCreateDto addressDTO) throws Exception {
        Address address = new Address(addressDTO);
        Person person = jpaPersonRepository.findById(addressDTO.getPersonId()).orElseThrow();
        address.setPerson(person);
        return jpaAddressRepository.save(address);
    }

    @Override
    public List<Address> listAddresses() {
        return jpaAddressRepository.findAll();
    }
}
