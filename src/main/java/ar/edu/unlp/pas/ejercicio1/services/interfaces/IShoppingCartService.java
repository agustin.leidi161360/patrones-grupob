package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import ar.edu.unlp.pas.ejercicio1.models.ShoppingCart;

public interface IShoppingCartService {

    ShoppingCart getShoppingCart(Long personId);

    Boolean clearShoppingCart(Long personId);

    Boolean addProductToShopingCart(Long personId, Long publicationId);

    Boolean removeProductFromShoppingCart(Long personId, Long publicationId);
}
