package ar.edu.unlp.pas.ejercicio1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "ar.edu.unlp.pas.ejercicio1.repositories")
@EnableTransactionManagement
public class Ejercicio1Application {

    public static void main(String[] args) {
        SpringApplication.run(Ejercicio1Application.class, args);
    }

}
