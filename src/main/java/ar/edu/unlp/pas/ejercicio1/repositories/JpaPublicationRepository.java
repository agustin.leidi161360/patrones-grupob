package ar.edu.unlp.pas.ejercicio1.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.pas.ejercicio1.models.Publication;

@Repository
public interface JpaPublicationRepository extends JpaRepository<Publication, Long> {

    // @Query( value = "SELECT pub.id, pub.stock, pub.sell_price, pub.min_price, pub.max_price, pub.is_active, pub.product_id FROM Publication pub INNER JOIN Product prod ON (pub.product_id = prod.id) INNER JOIN Person p ON (p.id = prod.person_id) WHERE p.id=?1"
    @Query( value = "SELECT * FROM Publication pub INNER JOIN Product prod ON (pub.product_id = prod.id) INNER JOIN Person p ON (prod.person_id = p.id) WHERE p.id = ?1"
    , nativeQuery = true)
    List<Publication> findPublicationsByPersonId( @Param("personId") Integer personId);

    


    // @Query(value = "SELECT * FROM shopping_cart WHERE person_id = ?1", nativeQuery = true)

}
