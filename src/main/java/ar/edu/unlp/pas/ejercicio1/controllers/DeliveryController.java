package ar.edu.unlp.pas.ejercicio1.controllers;

import ar.edu.unlp.pas.ejercicio1.dtos.Delivery.DeliveryGetDto;
import ar.edu.unlp.pas.ejercicio1.models.Delivery;
import ar.edu.unlp.pas.ejercicio1.services.implementations.DeliveryService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestController
public class DeliveryController {
    @Inject
    private DeliveryService deliveryService;

    @GetMapping("/api/deliveries")
    public ResponseEntity<List<DeliveryGetDto>> getDeliveries() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        long personId = Long.parseLong(String.valueOf(auth.getPrincipal()));
        List<Delivery> deliveries = deliveryService.getAllDeliveriesByPersonId(personId);
        List<DeliveryGetDto> deliveriesGetDto = new ArrayList<>();
        for (Delivery delivery : deliveries) {
            DeliveryGetDto deliveryGetDto = new DeliveryGetDto(delivery.getId(), delivery.getDateDelivery());
            deliveriesGetDto.add(deliveryGetDto);
        }
        return ResponseEntity.ok(deliveriesGetDto);
    }

    @GetMapping("/api/deliveries/{personId}")
    public ResponseEntity<List<DeliveryGetDto>> listDeliveriesByPersonId(@PathVariable("personId") Integer personId) {
        List<Delivery> deliveries = deliveryService.getAllDeliveriesByPersonId(personId);
        List<DeliveryGetDto> deliveriesGetDto = new ArrayList<>();
        for (Delivery delivery : deliveries) {
            DeliveryGetDto deliveryGetDto = new DeliveryGetDto(delivery.getId(), delivery.getDateDelivery());
            deliveriesGetDto.add(deliveryGetDto);
        }

        return ResponseEntity.ok(deliveriesGetDto);
    }
}
