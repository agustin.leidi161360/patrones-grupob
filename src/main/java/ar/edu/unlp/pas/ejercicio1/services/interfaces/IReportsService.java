package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import java.util.Date;

import ar.edu.unlp.pas.ejercicio1.dtos.Reports.ReportSalesByMonthDTO;

public interface IReportsService {

    ReportSalesByMonthDTO reportSalesByMonth(Integer month, Integer year);

    Integer reportSalesBetweenAge(int minAge, int maxAge);

    Integer reportAmount(
            Integer productId,
            String category,
            Integer buyerId,
            Integer sellerId,
            Date dateStart,
            Date dateEnd);
}
