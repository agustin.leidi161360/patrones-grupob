package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import java.util.List;
import java.util.Set;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationCreateDto;
import ar.edu.unlp.pas.ejercicio1.models.Publication;

public interface IPublicationService {

    public List<Publication> getPublications();

    public List<Publication> getPublicationsOfPerson(Integer idPerson);

    public Publication addPublication(PublicationCreateDto publication);

    public Publication modifyStock(Integer productId, Integer cantProducts);

    public Publication suspendPublication(Long idPublication);
    
    
}
