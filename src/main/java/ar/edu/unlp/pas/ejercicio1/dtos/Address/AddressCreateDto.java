package ar.edu.unlp.pas.ejercicio1.dtos.Address;

import javax.validation.constraints.NotNull;

public class AddressCreateDto {

    @NotNull(message = "La calle es obligatoria")
    private Integer street;

    @NotNull(message = "El numero es obligatorio")
    private Integer houseNumber;

    private Integer floorNumber;

    // Podria ser string
    private Integer department;

    @NotNull(message = "La localidad es obligatorio")
    private String city;

    @NotNull(message = "La provincia es obligatorio")
    private String province;

    @NotNull(message = "El numero es obligatorio")
    private String country;

    @NotNull(message = "La persona es obligatoria")
    private Long person_id;

    public AddressCreateDto() {
    }

    public AddressCreateDto(Integer street, Integer houseNumber, Integer floorNumber, Integer department,
            String city, String province, String country, Long person_id) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.floorNumber = floorNumber;
        this.department = department;
        this.city = city;
        this.province = province;
        this.country = country;
        this.person_id = person_id;
    }

    public AddressCreateDto(Integer street, Integer houseNumber, Integer floorNumber, Integer department,
                            String city, String province, String country) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.floorNumber = floorNumber;
        this.department = department;
        this.city = city;
        this.province = province;
        this.country = country;
    }

    public Integer getStreet() {
        return street;
    }

    public void setStreet(Integer street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(Integer floorNumber) {
        this.floorNumber = floorNumber;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getPersonId() {
        return person_id;
    }

    public void setPersonId(Long person_id) {
        this.person_id = person_id;
    }

    public Long getPerson_id() {
        return person_id;
    }

    public void setPerson_id(Long person_id) {
        this.person_id = person_id;
    }
}
