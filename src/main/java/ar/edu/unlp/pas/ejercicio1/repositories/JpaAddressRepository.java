package ar.edu.unlp.pas.ejercicio1.repositories;

import ar.edu.unlp.pas.ejercicio1.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaAddressRepository extends JpaRepository<Address, Long> {
}
