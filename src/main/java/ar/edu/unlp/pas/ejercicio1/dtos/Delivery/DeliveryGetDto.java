package ar.edu.unlp.pas.ejercicio1.dtos.Delivery;

import java.util.Date;

public class DeliveryGetDto {

    private Long id;
    private Date dateDelivery;

    public DeliveryGetDto(Long id, Date dateDelivery) {
        this.id = id;
        this.dateDelivery = dateDelivery;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }
}
