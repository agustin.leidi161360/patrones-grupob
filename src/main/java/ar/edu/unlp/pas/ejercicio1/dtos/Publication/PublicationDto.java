package ar.edu.unlp.pas.ejercicio1.dtos.Publication;

import ar.edu.unlp.pas.ejercicio1.models.Publication;

public class PublicationDto {

    private Long id;

    private Integer stock;
    private Double minPrice;
    private Double maxPrice;
    private Boolean isActive;

    public PublicationDto() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public PublicationDto(Publication publication) {
        this.id = publication.getId();
        this.stock = publication.getStock();
        this.minPrice = publication.getMinPrice();
        this.maxPrice = publication.getMaxPrice();
        this.isActive = publication.getIsActive();
    }

}
