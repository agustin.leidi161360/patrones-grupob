package ar.edu.unlp.pas.ejercicio1.services.implementations;

import ar.edu.unlp.pas.ejercicio1.models.Checkout;
import ar.edu.unlp.pas.ejercicio1.models.Delivery;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaDeliveryRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.IDeliveryService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Service
public class DeliveryService implements IDeliveryService {

    @Inject
    private JpaDeliveryRepository deliveryRepository;
    @Inject
    private CheckoutService checkoutService;
    @Inject
    private PersonService personService;

    @Override
    public Delivery getDeliveryById(long id) {
        return deliveryRepository.findById(id).orElse(null);
    }

    @Override
    public List<Delivery> getDeliveries() {
        return deliveryRepository.findAll();
    }

    @Override
    public List<Delivery> getAllDeliveriesByPersonId(long personId) {
        return deliveryRepository.findByPersonId(personId);
    }

    @Override
    public Set<Publication> getAllPublicationsByDeliveryId(long deliveryId) {
        Delivery delivery = deliveryRepository.findById(deliveryId).orElse(null);
        if (delivery == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No existe el delivery");
        }
        Checkout checkout = delivery.getCheckout();

        return checkout.getPublications();
    }
}
