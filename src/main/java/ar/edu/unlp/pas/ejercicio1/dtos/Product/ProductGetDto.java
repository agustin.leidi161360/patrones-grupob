package ar.edu.unlp.pas.ejercicio1.dtos.Product;

import ar.edu.unlp.pas.ejercicio1.models.Product;

public class ProductGetDto {

    private String name;

    private Double price;

    private String description;

    private String category;

    private Long addressId;


    private Long personId;

    


    public ProductGetDto(Product prod) {
        this.name = prod.getName();
        this.price = prod.getPrice();
        this.description = prod.getDescription();
        this.category = prod.getCategory();
        this.addressId = prod.getAddress().getId();
        this.personId = prod.getPerson().getId();
    }


    public ProductGetDto() {
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Double getPrice() {
        return price;
    }


    public void setPrice(Double price) {
        this.price = price;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getCategory() {
        return category;
    }


    public void setCategory(String category) {
        this.category = category;
    }


    public Long getAddressId() {
        return addressId;
    }


    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }


    public Long getPersonId() {
        return personId;
    }


    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    
}
