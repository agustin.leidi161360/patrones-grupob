package ar.edu.unlp.pas.ejercicio1.services.implementations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationDto;
import ar.edu.unlp.pas.ejercicio1.dtos.ShoppingCart.ShoppingCartDto;
import ar.edu.unlp.pas.ejercicio1.models.Person;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.models.ShoppingCart;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaPersonRepository;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaPublicationRepository;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaShoppingCartRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.IShoppingCartService;

@Service
@Transactional
public class ShoppingCartService implements IShoppingCartService {

    @Inject
    private JpaShoppingCartRepository shoppingCartRepository;

    @Inject
    private JpaPersonRepository personRepository;

    @Inject
    private JpaPublicationRepository publicationRepository;

    @Override
    public ShoppingCart getShoppingCart(Long personId) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByPersonId(personId.longValue());
        if (shoppingCart == null) {
            Person person = personRepository.findById(personId).orElse(null);

            if (person != null) {
                shoppingCart = this.createShoppingCart(person);
            } else {
                return null;
            }
        }
        return shoppingCart;
    }

    @Override
    public Boolean clearShoppingCart(Long personId) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByPersonId(personId.longValue());
        if (shoppingCart == null) {
            return false;
        }
        shoppingCart.getPublications().clear();
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    @Override
    public Boolean addProductToShopingCart(Long personId, Long publicationId) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByPersonId(personId.longValue());
        if (shoppingCart == null) {
            Person person = personRepository.findById(personId).orElse(null);
            shoppingCart = this.createShoppingCart(person);
        }
        Publication publication = publicationRepository.findById(publicationId).orElse(null);
        shoppingCart.addPublication(publication);
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    @Override
    public Boolean removeProductFromShoppingCart(Long personId, Long publicationId) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByPersonId(personId.longValue());
        if (shoppingCart == null) {
            return false;
        }
        Publication publication = publicationRepository.findById(publicationId).orElse(null);
        shoppingCart.removePublication(publication);
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    private ShoppingCart createShoppingCart(Person person) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setPerson(person);
        shoppingCartRepository.save(shoppingCart);
        return shoppingCart;
    }
}