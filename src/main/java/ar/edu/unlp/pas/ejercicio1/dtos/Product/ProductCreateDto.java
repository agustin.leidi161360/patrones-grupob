package ar.edu.unlp.pas.ejercicio1.dtos.Product;

import javax.validation.constraints.NotNull;

import ar.edu.unlp.pas.ejercicio1.models.Person;

public class ProductCreateDto {

    @NotNull(message="El campo nombre no debe ser nulo")
    private String name;

    @NotNull(message="El campo descripcion no debe ser nulo")
    private String description;

    @NotNull(message="El campo precio no debe ser nulo")
    private Double price;

    @NotNull(message="El campo categoria no debe ser nulo")
    private String category;

    @NotNull(message="Debe ingresar al vendedor")
    private Long personId;
    // private Address address; Se elige dentro de la implementacion del servicio una direccion aleatoria del vendedor.
    
    public ProductCreateDto(String name,
            String description,
            Double price,
            String category,
            Long personId) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.personId = personId;
    }

//     public ProductCreateDto(String name,
//     String description,
//     Double price,
//     String category,
//     Long personId) {
// this.name = name;
// this.description = description;
// this.price = price;
// this.category = category;
// this.personId = personId;
// }




    public String getName() {
        return name;
    }

    public ProductCreateDto() {
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public Long getPersonId() {
        return this.personId;
    }
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    
    
}
