package ar.edu.unlp.pas.ejercicio1.repositories;

import ar.edu.unlp.pas.ejercicio1.models.SystemConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaSystemConfigRepository extends JpaRepository<SystemConfig, Long> {
}
