package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import ar.edu.unlp.pas.ejercicio1.dtos.Address.AddressCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Address.AddressGetDto;
import ar.edu.unlp.pas.ejercicio1.models.Address;

import java.util.List;

public interface IAddressService {

    Address createAddress(AddressCreateDto address) throws Exception;

    List<Address> listAddresses();

}
