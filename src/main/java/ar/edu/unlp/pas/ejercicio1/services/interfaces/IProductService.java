package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import java.util.List;

import ar.edu.unlp.pas.ejercicio1.dtos.Product.ProductCreateDto;
import ar.edu.unlp.pas.ejercicio1.models.Product;

public interface IProductService {

    public List<Product> getProducts();


    public Product addProduct(ProductCreateDto p);
    
    
    
}
