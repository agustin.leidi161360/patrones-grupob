package ar.edu.unlp.pas.ejercicio1.services.implementations;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import ar.edu.unlp.pas.ejercicio1.dtos.Product.ProductCreateDto;
import ar.edu.unlp.pas.ejercicio1.models.Person;
import ar.edu.unlp.pas.ejercicio1.models.Product;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaPersonRepository;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaProductRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.IProductService;

@Service
public class ProductService implements IProductService {

    @Inject
    private JpaPersonRepository personRepository;
    @Inject
    private JpaProductRepository productRepository;

    public List<Product> getProducts(){
        List<Product> products = this.productRepository.findAll();
        return products;
     }

    public Product addProduct(ProductCreateDto prod) { 

        // valido que el vendedor ingresado exista
        Optional<Person> p = this.personRepository.findById(prod.getPersonId());
        Person person = p.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "{'personId': No existe la persona ingresada}"));

        Product product = new Product(prod.getName(), prod.getPrice(), prod.getDescription(), prod.getCategory(), person.getBillingAddress(), person);
        return productRepository.save(product);
    }
    
}
