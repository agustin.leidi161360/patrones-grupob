package ar.edu.unlp.pas.ejercicio1.services.implementations;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import ar.edu.unlp.pas.ejercicio1.dtos.Publication.PublicationCreateDto;
import ar.edu.unlp.pas.ejercicio1.models.Product;
import ar.edu.unlp.pas.ejercicio1.models.Publication;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaProductRepository;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaPublicationRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.IPublicationService;

@Service
public class PublicationService implements IPublicationService {

    @Inject
    private JpaPublicationRepository pubRepository;
    @Inject
    private JpaProductRepository prodRepository;

 
    public List<Publication> getPublications(){
       List<Publication> publications = this.pubRepository.findAll();
       return publications;
    }

    public Publication addPublication(PublicationCreateDto publicationCreateDto) { 

        Optional<Product> optProduct = this.prodRepository.findById((publicationCreateDto.getProductId().longValue()));
        Product prod = optProduct.orElseThrow( () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Debe ingresar un producto existente") );
        Publication publication = new Publication(publicationCreateDto.getStock(), publicationCreateDto.getSellPrice() , publicationCreateDto.getMinPrice(), publicationCreateDto.getMaxPrice(), publicationCreateDto.getIsActive(), prod);
        return pubRepository.save(publication);
        
    }



    
    public Publication modifyStock(Integer publicationId, Integer cantProducts) {


        Publication publication = pubRepository.findById(publicationId.longValue()).orElseThrow(() -> new ResponseStatusException( HttpStatus.BAD_REQUEST, "La publicacion no existe"));
        if (cantProducts < 0) { // Si es menor a 0 , es una resta. Podria ser una resta del stock que hay o una suma.
            if ( (publication.getStock() + cantProducts) < 0 ) { 
                  throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "La resta de stock solicitada supera al stock disponible del producto");
            }    
            
        }
    
        publication.setStock(publication.getStock() + cantProducts);
        return pubRepository.save(publication);
    }


    public Publication suspendPublication(Long idPublication) { 
        Publication pub = pubRepository.findById(idPublication).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if(!pub.getIsActive()){ 
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La publicacion ya se encuentra inactiva");
        }
        pub.setIsActive(false);
        return pubRepository.save(pub);


    }

    @Override
    public List<Publication> getPublicationsOfPerson(Integer idPerson) {

        return pubRepository.findPublicationsByPersonId(idPerson);
        // TODO Auto-generated method stub
    }
    
    
}
