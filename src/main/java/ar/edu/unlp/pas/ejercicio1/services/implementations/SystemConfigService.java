package ar.edu.unlp.pas.ejercicio1.services.implementations;

import ar.edu.unlp.pas.ejercicio1.dtos.SystemConfig.SystemConfigGetDto;
import ar.edu.unlp.pas.ejercicio1.dtos.SystemConfig.SystemConfigUpdateDto;
import ar.edu.unlp.pas.ejercicio1.models.SystemConfig;
import ar.edu.unlp.pas.ejercicio1.repositories.JpaSystemConfigRepository;
import ar.edu.unlp.pas.ejercicio1.services.interfaces.ISystemConfigService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;

@Service
@Transactional
public class SystemConfigService implements ISystemConfigService {

    @Inject
    JpaSystemConfigRepository systemConfigRepository;

    @Override
    public SystemConfig createSystemConfig(SystemConfig systemConfig) {
        return systemConfigRepository.save(systemConfig);
    }

    @Override
    public SystemConfig updateSystemConfig(SystemConfigUpdateDto systemConfigUpdateDto) {
        SystemConfig systemConfig = systemConfigRepository.findAll().get(0);
        systemConfig.setCommission(systemConfigUpdateDto.getCommission());
        systemConfig.setMaxOpinionsToShow(systemConfigUpdateDto.getMaxOpinionsToShow());
        return systemConfigRepository.save(systemConfig);
    }

    @Override
    public SystemConfig getSystemConfig() {
        return systemConfigRepository.findAll().get(0);
    }
}
