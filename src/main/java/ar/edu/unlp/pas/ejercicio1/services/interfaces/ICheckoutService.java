package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import ar.edu.unlp.pas.ejercicio1.models.Checkout;

import java.util.List;

public interface ICheckoutService {
    Checkout createCheckout(long personId);

    Checkout getCheckoutById(long id);

    List<Checkout> getAllCheckoutsByPersonId(long id);
}
