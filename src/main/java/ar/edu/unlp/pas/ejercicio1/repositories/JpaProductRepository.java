package ar.edu.unlp.pas.ejercicio1.repositories;

import org.springframework.stereotype.Repository;

import ar.edu.unlp.pas.ejercicio1.models.Product;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface JpaProductRepository extends JpaRepository<Product, Long> { 

    
    
}
