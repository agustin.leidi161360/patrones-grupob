package ar.edu.unlp.pas.ejercicio1.repositories;

import ar.edu.unlp.pas.ejercicio1.dtos.Reports.ReportSalesByMonthDTO;
import ar.edu.unlp.pas.ejercicio1.models.Checkout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaCheckoutRepository extends JpaRepository<Checkout, Long> {
    List<Checkout> findByPersonId(long personId);

    @Query(value = "SELECT EXTRACT(MONTH FROM date) AS " + '"' + "MONTH" + '"' + ", count(*) AS SALES " +
            "FROM Checkout " +
            "RIGHT JOIN PUBLICATION_CHECKOUT ON PUBLICATION_CHECKOUT.checkout_id = CHECKOUT.id " +
            "WHERE to_char(date, 'YYYY') = :year " +
            "AND to_char(date, 'MM') = :month " +
            "GROUP BY EXTRACT(MONTH FROM date) " +
            "ORDER BY EXTRACT(MONTH FROM date) ASC", nativeQuery = true)
    ReportSalesByMonthDTO reportSalesByMonth(@Param("month") int month, @Param("year") int year);

    @Query(value = "SELECT count(*) FROM Checkout" +
            " RIGHT JOIN PUBLICATION_CHECKOUT ON PUBLICATION_CHECKOUT.checkout_id = CHECKOUT.id " +
            "LEFT JOIN PERSON ON PERSON.ID= CHECKOUT.person_id " +
            "WHERE cast(DATEDIFF(dd,BIRTH, now())/365.25 as int) BETWEEN :minAge AND :maxAge ", nativeQuery = true)
    Integer reportSalesBetweenAge(@Param("minAge") int minAge, @Param("maxAge") int maxAge);

}
