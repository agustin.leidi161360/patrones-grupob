package ar.edu.unlp.pas.ejercicio1.controllers;

import ar.edu.unlp.pas.ejercicio1.dtos.Address.AddressCreateDto;
import ar.edu.unlp.pas.ejercicio1.dtos.Address.AddressGetDto;
import ar.edu.unlp.pas.ejercicio1.models.Address;
import ar.edu.unlp.pas.ejercicio1.services.implementations.AddressService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AddressController {

    @Inject
    private AddressService addressService;

    @PostMapping("/api/address")
    public ResponseEntity<Object> createAddress(
            @Valid @RequestBody AddressCreateDto addressDto) {
        try {
            Address address = addressService.createAddress(addressDto);
            AddressGetDto addressGetDto = new AddressGetDto(address);
            return ResponseEntity.ok(addressGetDto);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/api/address")
    public ResponseEntity<List<AddressGetDto>> listAddress() {
        List<Address> addresses = addressService.listAddresses();
        List<AddressGetDto> addressesGetDto = new ArrayList<>();
        for (Address address : addresses) {
            AddressGetDto addressGetDTO = new AddressGetDto(address);
            addressesGetDto.add(addressGetDTO);
        }
        return ResponseEntity.ok(addressesGetDto);
    }

}
