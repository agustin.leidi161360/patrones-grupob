package ar.edu.unlp.pas.ejercicio1.repositories.implementations;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

@Service
public class JpaReportsRepositoryImp {

    @PersistenceContext
    private EntityManager entityManager;

    public Integer reportAmount(
            Integer productId,
            String category,
            Integer buyerId,
            Integer sellerId,
            Date dateStart,
            Date dateEnd) {
        String query = "SELECT SUM(PUB.SELL_PRICE) FROM CHECKOUT CH " +
                "LEFT JOIN PUBLICATION_CHECKOUT PCH ON CH.ID = PCH.CHECKOUT_ID " +
                "LEFT JOIN PUBLICATION PUB ON PUB.ID = PCH.PUBLICATION_ID " +
                "LEFT JOIN PRODUCT P ON P.ID = PUB.PRODUCT_ID ";
        // "WHERE 1=1 "; // el 1 igual a 1 es para siempre haya una condición en el
        // where y que no rompa
        // con un AND sin el where
        if (productId != null) {
            query += " and p.id = :productId";
        }
        if (category != null) {
            query += " and p.category = :category";
        }
        if (buyerId != null) {
            query += " and ch.person_id = :buyerId";
        }
        if (sellerId != null) {
            query += " and p.person_id = :sellerId";
        }
        if (dateStart != null && dateEnd != null) {
            query += " and ch.date between :dateStart and :dateEnd";
        }
        System.out.println(query);
        javax.persistence.Query q = entityManager.createNativeQuery(query);
        if (productId != null) {
            q.setParameter("productId", productId);
        }
        if (category != null) {
            q.setParameter("category", category);
        }
        if (buyerId != null) {
            q.setParameter("buyerId", buyerId);
        }
        if (sellerId != null) {
            q.setParameter("sellerId", sellerId);
        }
        if (dateStart != null && dateEnd != null) {
            q.setParameter("dateStart", dateStart);
            q.setParameter("dateEnd", dateEnd);
        }
        try {
            BigDecimal result = (BigDecimal) q.getSingleResult();
            if (result == null) {
                return 0;
            }
            return result.intValue();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }

    }

}
