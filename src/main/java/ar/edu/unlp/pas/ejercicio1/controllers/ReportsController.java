package ar.edu.unlp.pas.ejercicio1.controllers;

import java.util.Date;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unlp.pas.ejercicio1.dtos.Reports.ReportSalesByMonthDTO;
import ar.edu.unlp.pas.ejercicio1.services.implementations.ReportsService;

@RestController
public class ReportsController {

    @Inject
    ReportsService reportService;

    @GetMapping("/api/reports/sales")
    public ResponseEntity<ReportSalesByMonthDTO> reportSalesByMonth(
            @RequestParam(value = "month", required = true) Integer month,
            @RequestParam(value = "year", required = true) Integer year) {
        try {
            return ResponseEntity.ok(reportService.reportSalesByMonth(month, year));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/api/reports/sales/by-age")
    public ResponseEntity<Integer> reportSalesByAge(
            @RequestParam(value = "minAge", required = true) Integer minAge,
            @RequestParam(value = "maxAge", required = true) Integer maxAge) {
        try {
            return ResponseEntity.ok(reportService.reportSalesBetweenAge(minAge, maxAge));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/api/reports/amount")
    public ResponseEntity<?> reportAmount(
            @RequestParam(value = "productId", required = false) Integer productId,
            @RequestParam(value = "category", required = false) String category,
            @RequestParam(value = "buyerId", required = false) Integer buyerId,
            @RequestParam(value = "sellerId", required = false) Integer sellerId,
            @RequestParam(value = "dateStart", required = false) Date dateStart,
            @RequestParam(value = "dateEnd", required = false) Date dateEnd) {
        try {
            return ResponseEntity.ok(reportService.reportAmount(
                    productId,
                    category,
                    buyerId,
                    sellerId,
                    dateStart,
                    dateEnd));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}