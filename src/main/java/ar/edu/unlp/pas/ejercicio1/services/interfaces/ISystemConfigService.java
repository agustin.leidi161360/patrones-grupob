package ar.edu.unlp.pas.ejercicio1.services.interfaces;

import ar.edu.unlp.pas.ejercicio1.dtos.SystemConfig.SystemConfigUpdateDto;
import ar.edu.unlp.pas.ejercicio1.models.SystemConfig;

public interface ISystemConfigService {

    SystemConfig createSystemConfig(SystemConfig systemConfig);

    SystemConfig updateSystemConfig(SystemConfigUpdateDto systemConfigUpdateDto);

    SystemConfig getSystemConfig();
}
