## [1.0] - 3-5-2023
### Added
- Se implementó la autenticación y la seguridad de la aplicación mediante el uso de SpringcSecurity y se agregaron los siguientes roles:
  - ADMIN: puede leer y escribir los datos.
  - USUARIO: puede solamente leer los datos.
- Se actualizó el dominio para manejar la relación entre Persona y sus direcciones de envío y facturación.
- Se actualizaron las validaciones y propiedades de la entidad Persona, como apellido, nombre, fecha de nacimiento y email.
  - Apellido: no puede ser nulo
  - Nombre: no puede ser nulo
  - Fecha Nacimiento: debe ser una fecha válida, la persona no puede ser mayor a 125 años ni menor a 18 años.
  - Email: no puede ser nulo y debe ser un mail válido, y debe ser único en el sistema.
- Se creó una entidad Dirección con propiedades como calle, número, piso, departamento, localidad, provincia y país.
- Creación de controladores que definen los endpoints para las operaciones que exporta el servicio, siguiendo criterios REST.
- Se implementó un inicializador que carga algunos datos de prueba en el sistema.
- Se creó un servicio transformador que convierte una entidad de la base de datos en el DTO definido anteriormente, y otro servicio que implementa las operaciones CRUD y necesita inyectar el repositorio definido en el punto 4.
- Se definió un repositorio de JPA para las personas, que extiende la interfaz JpaRepository.
- Se creó una clase DTO para la entidad Persona que contiene los atributos ID, nombre completo (concatenación de apellidos y nombres) y edad (calculada a partir de la fecha de nacimiento).
- Se creó una entidad Persona con atributos como ID, apellidos y nombres, y fecha de nacimiento, y se implementaron operaciones CRUD sobre esta entidad.
- Se creó un proyecto de Spring Boot y se configuró la conexión a la base de datos H2.